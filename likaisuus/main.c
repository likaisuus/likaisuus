/*
 * main.c
 *
 *
 *  TODO:
 *      Invert output
 *      Filtering measurements
 *      Save input to flash
 *      Calculate average based on saved input values
 *
 *
 *
 *
 *  P2.2 is used as PWM output
 *  P1.0 reads analog signal from optical sensor
 *  P1.5 jumper option for test (GND = TEST; Vcc = RELEASE)
 *  P1.3 Light control
 *
 */


#include <msp430g2553.h>
#include <inttypes.h>

#include "adc.h"

// 1 timer cycle -> 65535 / 8 000 000 seconds
// COUNTER MAX 100 ~= 0.82s
// Determines delay between input readings
#define ADC_2_PWM_MULT 7.8125
#define ADC_BUF_SIZE 10
#define MES_TIMER_MAX_TEST 61      // 610 == 5s
#define MES_TIMER_MAX_LONG 61     // 9150 == 75s

#define SENSOR_MAX_V 3.00
#define SENSOR_MIN_V 1.00
#define SENSOR_MIN_ADC10 (SENSOR_MIN_V * 341.33)

void init_board();
void poll_timing_jumper();


volatile uint16_t new_pwm_duty = 0;     // Updated pwm duty cycle value
//volatile uint16_t timer_counter = 0;    // Counter for input reading delay

volatile uint8_t pwm_update_flag = 0;   // flag to determine when to update timer register for new pwm value
volatile uint8_t get_input_flag = 0;    // flag to read input

volatile uint16_t timer1_counter = 0;	// Counter for adc measurement delays
volatile uint16_t t1_counter_max = MES_TIMER_MAX_TEST;	// Change to determine time between measurements

uint16_t adc_buf[ADC_BUF_SIZE] = {0};             // Buffer to calculate ADC input average over 10 measurements
uint8_t adc_buf_idx = 0;

uint16_t avg = 0;
uint16_t sum = 0;

int main(void) {

    init_board();
    init_adc();
    init_timer();

    __enable_interrupt();


    while(1) {

    	poll_timing_jumper();

        if (get_input_flag == 1) {
            uint16_t adc_value = 0;
            get_input_flag = 0;


            P1OUT |= BIT3;
            uint8_t n;
            for(n = 0; n < ADC_BUF_SIZE; ++n) {
                // Lights on for measurememnts

                // Start conversion and after ADC10 ISR read adc10mem for new value
                adc_start_conv();
                adc_value = ADC10MEM;
                // Lights off



                adc_buf[n] = adc_value;

            }
            P1OUT &= ~BIT3;


/*
            // Store ADC values to buffer and calculate average
            adc_buf[adc_buf_idx] = adc_value;
            adc_buf_idx++;
            if (adc_buf_idx >= ADC_BUF_SIZE) { adc_buf_idx = 0; }
*/


            // Calculate average of measurements
            uint8_t i;


            for(i = 0; i < ADC_BUF_SIZE; ++i) {
                sum += adc_buf[i];
            }
            avg = (uint16_t)(sum * 0.1);
            sum = 0;


            // Scale for expected min and max values from sensor

            if (avg < SENSOR_MIN_ADC10) { avg = 0; }
            else { avg -= SENSOR_MIN_ADC10; }
            avg *= (3 / (SENSOR_MAX_V - SENSOR_MIN_V));
            if (avg > 1024) { avg = 1024; }

            // Invert output
            avg = 1024 - avg;


            // Might reduce some flicker or not
            //if (adc_value < 10) { adc_value = 0; }

            pwm_update_flag = 1;

            // CHANGE COMMENTED LINE TO REVERT FROM AVERAGE CALCULATING
            //new_pwm_duty = (uint16_t)(adc_value * ADC_2_PWM_MULT);  // Scale adc input to pwm duty cycle value
            new_pwm_duty = (uint16_t)(avg * ADC_2_PWM_MULT);  // Scale adc input to pwm duty cycle value
        }

    }

    return 0;
}

/*
 * Initialize clock and other stuff before specific peripherals
 */
void init_board() {
    WDTCTL = WDTPW | WDTHOLD;       // Stop watchdog timer
    //P1DIR |= 0x01;                    // Set P1.0 to output direction
    // Set DCO to 8Mhz and delay to let it set
    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;
    __delay_cycles(50000);

    // P1.3, P1.4 set as outputs for diode control
    P1DIR |= BIT3;
    P1DIR &= ~BIT5;
}

void poll_timing_jumper() {
	uint16_t in = P1IN & BIT5;
	static uint16_t in_old;	// Makes sure values are only changed once
	if (in == 0x00 && in_old == BIT5) {
		// Set timing to ~ 24 / second
		// Make sure counter value stays below max value
		if (t1_counter_max != MES_TIMER_MAX_TEST) {
			timer1_counter = 0;
		}
		t1_counter_max = MES_TIMER_MAX_TEST;
		TA0CTL |= ID_0;

	} else if (in == BIT5 && in_old == 0x00) {
		// Set timing to ~ 1 / min (sets to true value in 10min)
		t1_counter_max = MES_TIMER_MAX_LONG;
		TA0CTL |= ID_0;
	} /*else {	// Should figure something else to fill here
		if (t1_counter_max != MES_TIMER_MAX_TEST) {
			timer1_counter = 0;
		}
		t1_counter_max = MES_TIMER_MAX_TEST;
		TA0CTL |= ID_0;
	}*/
	in_old = in;
}

#pragma vector = TIMER0_A0_VECTOR
__interrupt void TA0_ISR(void) {
    // Counter for timing input handling
	timer1_counter++;
	if (timer1_counter == t1_counter_max) {
		get_input_flag = 1;
		timer1_counter = 0;
	}
}

#pragma vector = TIMER1_A0_VECTOR
__interrupt void TA1_ISR(void){
    // Check for changed PWM value at counter overflow
    if (pwm_update_flag == 1) {
        TA1CCR1 = new_pwm_duty;
        pwm_update_flag = 0;
    }
}


// Used to return from power save after adc conversion is done
#pragma vector = ADC10_VECTOR
__interrupt void adc10_isr(void) {
    __bic_SR_register_on_exit(CPUOFF);
}


