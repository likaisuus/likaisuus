/*
 * adc.h
 *
 *  Created on: 12.1.2017
 *      Author: Joonas
 */

#ifndef ADC_H_
#define ADC_H_

#include <msp430g2553.h>
#include <inttypes.h>

void init_adc();
void adc_start_conv();
void init_timer();

#endif /* ADC_H_ */
