/*
 * adc.c
 *
 *  Created on: 12.1.2017
 *      Author: Joonas
 */

#include "adc.h"

/*
 * Initialize adc10 for single read in pin 1.0
 */
void init_adc() {
    ADC10CTL0 = ADC10ON + ADC10IE;
    ADC10CTL1 = ADC10DIV_7;
    ADC10AE0 = INCH_0;
}

/*
 * Starts conversion and enters power save to pause program while adc conversion is ongoing
 * Interrupt when conversion is done and adc10mem can be read
 */
void adc_start_conv() {
    ADC10CTL0 |= ENC + ADC10SC;         // Start conversion
    __bis_SR_register(CPUOFF + GIE);    // Go to LPM0, interrupt when conversion finished
}


// Initialize timer A for PWM generation
// Update PWM duty cycle by changing value of TA0CCR1
// 0xFFFF -> 0%,      0x0000 -> 100%
void init_timer() {
    // P1.2 as timer out
    P2DIR |= BIT2;
    P2SEL |= BIT2;


    // TIMER1 INIT (PWM)
    // Compare register for pwm duty cycle selection
    TA1CCR0 = 8000;
    // Counting to this value changes output to 1 and counting to ccr0 changes output to 0
    //TA0CCR1 = 0x0000;
    // SMCLK, div 1, Continuous mode, Interrupt enable
    TA1CTL |= TASSEL_2 + ID_0 + MC_1;
    TA1CCTL0 |= CCIE;
    TA1CCTL1 |= OUTMOD_7;

    // TIMER0 INIT (TIMING)
    TA0CCR0 = 0xFFFF;
    TA0CCTL0 |= CCIE;
    TA0CTL |= TASSEL_2 + ID_0 + MC_1;	// SMCLK, div2, up mode
    TA0CCTL1 |= OUTMOD_7;


}
